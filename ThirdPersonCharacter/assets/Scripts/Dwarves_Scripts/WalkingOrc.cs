﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput; 
public class WalkingOrc : MonoBehaviour
{

	public Animator animator;

	public float walkspeed = 5;
	private float horizontal;
	private float vertical;
	private float rotationDegreePerSecond = 1000;
	private bool isAttacking = false;

	public GameObject gamecam;
	public Vector2 camPosition;
	private bool dead;

	public GameObject[] characters;
	public int currentChar = 0;
	public bool isRangeAttack = false;

	public GameObject switchButton;
	public GameObject superPowerButton;
	public GameObject stunParticleEffect;
	public bool superPowerProgressBool;
	private Image superPowerProgress;
	public Text mainCharacterState;

	public detectHit_Main dh;
	private GameObject main_weapon;
	private Transform target = null;

	private List<Sprite> switchSpriteList;
	private List<Sprite> superPowerSpriteList;
	private List<float> superPowerTimeLeft;
	private List<string> superPowerState;
	private List<float> mainCharacterSpeedRate;

	private float startTime;

	void Start(){
		setCharacter(0);
		main_weapon = GameObject.FindWithTag ("Main_Weapon");
		main_weapon.SetActive (false);
		switchSpriteList = new List<Sprite>();
		Sprite tmpSprite= Resources.Load("bomb", typeof(Sprite)) as Sprite;
		switchSpriteList.Add (tmpSprite);
		tmpSprite= Resources.Load("hammer", typeof(Sprite)) as Sprite;
		switchSpriteList.Add (tmpSprite);
		tmpSprite= Resources.Load("arrow and bow 1", typeof(Sprite)) as Sprite;
		switchSpriteList.Add (tmpSprite);

		superPowerSpriteList = new List<Sprite>();
		tmpSprite= Resources.Load("thor-hammer", typeof(Sprite)) as Sprite;
		superPowerSpriteList.Add (tmpSprite);
		tmpSprite= Resources.Load("big-arraw", typeof(Sprite)) as Sprite;
		superPowerSpriteList.Add (tmpSprite);
		tmpSprite= Resources.Load("bsIcon", typeof(Sprite)) as Sprite;
		superPowerSpriteList.Add (tmpSprite);


		superPowerProgressBool = true;
		startTime = Time.time;

		superPowerProgress = superPowerButton.transform.Find ("CoolDownColor").gameObject.GetComponentInChildren<Image>();
		superPowerProgress.fillClockwise = false;
		superPowerProgress.fillAmount = 0;

		superPowerTimeLeft = new List<float> ();
		superPowerTimeLeft.Add (0);
		superPowerTimeLeft.Add (0);
		superPowerTimeLeft.Add (0);

		superPowerState = new List<string> ();
		superPowerState.Add ("Stunning the Ground Cooling Down");
		superPowerState.Add ("Shoot to Death");
		superPowerState.Add ("Boob Mania");

		mainCharacterSpeedRate = new List<float> ();
		mainCharacterSpeedRate.Add (1f);
		mainCharacterSpeedRate.Add (1.5f);
		mainCharacterSpeedRate.Add (0.5f);

		mainCharacterState.text = "";
	}

	void FixedUpdate()
	{
		if (animator && !dead)
		{
			//walk
			horizontal = CrossPlatformInputManager.GetAxis("Horizontal");
			vertical = CrossPlatformInputManager.GetAxis("Vertical");
			float speedOut;
			var cameraTransform = Camera.main.transform;
			// Forward vector relative to the camera along the x-z plane	
			var forward = cameraTransform.TransformDirection(Vector3.forward);
			forward.y = 0;
			forward = forward.normalized;
			var rightTmp = new Vector3(forward.z, 0, -forward.x);
			Vector3 stickDirection = horizontal * rightTmp + vertical * forward;
			if (stickDirection.sqrMagnitude > 1) stickDirection.Normalize();

			if (isAttacking||dh.stop)
				speedOut = 0;

			else
				speedOut = stickDirection.sqrMagnitude;

			if (stickDirection != Vector3.zero && !isAttacking)
				transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.LookRotation(stickDirection, Vector3.up), rotationDegreePerSecond * Time.deltaTime);
			Vector3 velocity = transform.forward * speedOut * walkspeed * mainCharacterSpeedRate[currentChar]+ new Vector3(0, GetComponent<Rigidbody>().velocity.y, 0);
			GetComponent<Rigidbody> ().velocity = velocity;
			animator.SetFloat("Speed", speedOut);

		}
	}

	public void Attack() {
		isAttacking = true;
		animator.SetTrigger("Attack");
		main_weapon.SetActive (true);
		StartCoroutine(stopAttack(1));
		activateTrails(true);
	}

	private IEnumerator warriorSuperPower(){
		stunParticleEffect.SetActive (true);
		this.Attack ();
		yield return new WaitForSeconds(2);
		stunParticleEffect.SetActive (false);
		mainCharacterState.text = "";
	}

	void Update()
	{
		if(dh.healthbar.value<=0){
			StartCoroutine (selfdestruct ());
			animator.SetFloat ("Speed", 0);
			return;
		}
		if (!dh.stop)
		{
			// move camera
			if (gamecam)
				gamecam.transform.position = transform.position + new Vector3(0, camPosition.x, -camPosition.y);

			// attack

			if (CrossPlatformInputManager.GetButtonUp ("Attack")) {
				detectHit_Main dm = gameObject.GetComponent<detectHit_Main>() as detectHit_Main;
				if (!isRangeAttack)
					Attack ();
				else {
					if (dm.Ammo_Check()) {
						Attack ();
						dm.Ammo_Dec();
					}
				}
			}


			animator.SetBool("isAttacking", isAttacking);

			//switch character
			if (CrossPlatformInputManager.GetButtonUp ("Switch")) {
				superPowerTimeLeft [currentChar] = superPowerProgress.fillAmount;
				setCharacter(-1);
				changeButtonImage ();
				superPowerProgress.fillAmount = superPowerTimeLeft [currentChar];
				/*GameObject switchButton = frontView.transform.Find("SwitchButton") as Button;
				switchButton.*/
			}
			if (CrossPlatformInputManager.GetButtonUp ("SuperPower")&&superPowerProgressBool) {
				superPowerProgressBool = false;
				//super power functions
				if (currentChar == 0) {
					StartCoroutine(warriorSuperPower ());
				} else {
					detectHit_Main dm = gameObject.GetComponent<detectHit_Main> () as detectHit_Main;
				}
				mainCharacterState.text =	superPowerState [currentChar];
				//cool down function
				//StartCoroutine(superPowerCoolDown);


				superPowerProgress.fillAmount = 1;
				startTime = Time.time;
				if (currentChar == 0) {
					GetComponent<ActivateStunEffect> ().Activate ();
				}
			}

			if(!superPowerProgressBool){
				superPowerProgress.fillAmount -= 1.0f/5000 * (Time.time - startTime);
				//print ("progress.fillAmount=" + superPowerProgress.fillAmount);
				if (superPowerProgress.fillAmount == 0) {
					superPowerProgressBool = true;
					mainCharacterState.text = "";
				}
			}
		}

	}

	public IEnumerator stopAttack(float lenght)
	{
		yield return new WaitForSeconds(lenght); // attack lenght
		isAttacking = false;
		activateTrails(false);
		main_weapon.SetActive (false);
	}

	public IEnumerator selfdestruct()
	{
		animator.SetTrigger("isDead");
		GetComponent<Rigidbody>().velocity = Vector3.zero;
		dead = true;

		yield return new WaitForSeconds(1.3f);
		GameObject.FindWithTag("GameController").GetComponent<gameContoller>().resetLevel();
	}

	public void setCharacter(int i)
	{
		currentChar += i;

		if (currentChar > characters.Length - 1)
			currentChar = 0;
		if (currentChar < 0)
			currentChar = characters.Length - 1;

		foreach (GameObject child in characters)
		{
			if (child == characters[currentChar])
				child.SetActive(true);
			else
			{
				child.SetActive(false);

				if (child.GetComponent<triggerProjectile>())
					child.GetComponent<triggerProjectile>().clearProjectiles();
			}
		}

		animator = GetComponentInChildren<Animator>();

		if (currentChar > 0)
			isRangeAttack = true;
		else
			isRangeAttack = false;
		
		detectHit_Main dm = gameObject.GetComponent<detectHit_Main>() as detectHit_Main;
		if (currentChar == 0) {
			dm.Ammo_Disappear ();
		} else {
			dm.Ammo_Appear ();
		}

	}

	public void SetTarget(Transform transform){
		//		Debug.Log ("setTarget");
		//		Debug.Log (transform.name);
		target = transform;
	}

	public Transform GetTarget(){
		return target;
	}


	public void activateTrails(bool state){
		var tails = GetComponentsInChildren<TrailRenderer>();
		foreach (TrailRenderer tt in tails)
		{
			tt.enabled = state;
		}
	}

	private void changeButtonImage(){
		var switchImage = switchButton.GetComponent<Image>();
		switchImage.sprite = switchSpriteList[currentChar];
		var superPowerImage = superPowerButton.GetComponent<Image>();
		superPowerImage.sprite = superPowerSpriteList[currentChar];

	}
}
