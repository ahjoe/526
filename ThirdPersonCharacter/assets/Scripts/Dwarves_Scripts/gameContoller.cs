﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class gameContoller : MonoBehaviour {

	//public Image darkness;
	public GameObject hazardSke;
	public GameObject hazardOrc;
	public GameObject healthPack;
	public GameObject ammoPack;
	public GameObject mainChar;
	public Text Secene_2_MainTime;
	public Text Secene_2_Round;
	//public GameStatistics gamestatistics;

	public bool waveStart;
	public int hazardCount;
	public int waveCount;
	public float spawnWait;
	public float startWait;
	public float waveWait;
	public Vector3 spawnValues;
	private List<GameObject> al;
	private List<List<GameObject>> ll;
	private int waveCnt;
	private float ti;
	private bool gameOver;
	private Vector3[] spawnPoints;
	private bool generatePack;
	void Start () {
		//darkness.enabled = true;
		//darkness.CrossFadeAlpha(1, 0, false);
		//darkness.CrossFadeAlpha(0, 0.5f, false);
		spawnPoints = new Vector3[6];
		spawnPoints [0] = new Vector3 (53f, 0f, 0f);
		spawnPoints [1] = new Vector3 (-55f, 0f, 0f);
		spawnPoints [2] = new Vector3 (-40f, 0f, -20f);
		spawnPoints [3] = new Vector3 (-30f, 0f, 24f);
		spawnPoints [4] = new Vector3 (40f, 0f, 20f);
		spawnPoints [5] = new Vector3 (30f, 0f, -30f);
		waveStart = true;
		gameOver = false;
		StartCoroutine (SpawnWaves ());
		waveCnt = 0;
		generatePack = false;
		ll = new List<List<GameObject>> ();
		for (int i = 0; i < waveCount; i++) {
			al = new List<GameObject>();
			GameObject hazardTmp;
			if (i % 2 == 0)
				hazardTmp = hazardSke;
			else
				hazardTmp = hazardOrc;
			for (int j = 0; j < hazardCount; j++) {
				Vector3 tmp = spawnPoints [Random.Range (0, 5)];
				Vector3 spawnPosition = tmp + new Vector3 (Random.Range(-spawnValues.x, spawnValues.x), spawnValues.y, Random.Range(-spawnValues.z, spawnValues.z));
				Quaternion spawnRotation = Quaternion.identity;
				GameObject tt = Instantiate (hazardTmp, spawnPosition, spawnRotation) as GameObject;
				al.Add (tt);
			}
			ll.Add (al);
		}
	}
	void Update(){
		detectHit_Main dm = mainChar.GetComponent<detectHit_Main> () as detectHit_Main;
		if (dm.healthbar.value <= 0) {
			Secene_2_Round.text = "Game Over";
			gameOver = true;
			float time = 4.0f;
			StartCoroutine(waitDeath(time));
		}
	}
	IEnumerator waitDeath(float time){
		yield return new WaitForSeconds(time);
		Application.LoadLevel(2);
	}

	public void resetLevel(){
		//darkness.CrossFadeAlpha(1, 0.5f, false);
		StartCoroutine(reload());		
	}

	public IEnumerator reload(){
		yield return new WaitForSeconds(0.5f);
		//Application.LoadLevel(Application.loadedLevelName);
	}

	IEnumerator SpawnWaves(){
		yield return waitSeconds(startWait);
		StartCoroutine (generateItems ());
		while ((waveCnt)<waveCount) {
			//Wave Start Prepare;
			List<GameObject> alTmp = ll[waveCnt++];
			yield return roundWaiter (3);
			//Spawn
			generatePack = false;
			Secene_2_MainTime.text = "";
			for (int i = 0; i < hazardCount; i++) {
				print ("SpawnWaves");
				GameStatistics mysta = GameStatistics.getInstance ();
				mysta.ht ["level"] = ((int) mysta.ht ["level"])+1;
				GameObject tt = alTmp [i];
				tt.transform.LookAt (Vector3.zero);
				tt.SetActive (true);
				yield return new WaitForSeconds(spawnWait);
			}
			yield return waitSeconds (waveWait);
			generatePack = true;
			//Wave Collect
			//Kill all the gameobjects in prev wave
			//alTmp.ForEach((GameObject tmp)=>{print("Destroy");Destroy(tmp);});
			//yield return waitSeconds (waveWait);
		}
		Application.LoadLevel(2);
	}

	private IEnumerator waitSeconds(float timeTmp){
		while ((timeTmp--) > 0) {
			print ("Time is "+ti);
			ti = timeTmp+1;
			Secene_2_MainTime.text = ((int)ti).ToString ();
			yield return new WaitForSeconds (1);
		}
	}

	private IEnumerator roundWaiter(float roundTime){
		if (!gameOver) {
			Secene_2_MainTime.text = "";
			Secene_2_Round.text = "WAVE " + (waveCnt).ToString ();
			yield return new WaitForSeconds (roundTime);
			Secene_2_Round.text = "";
		}
	}

	public IEnumerator generateItems(){
		yield return new WaitForSeconds(startWait);
		while (true) {
			if (generatePack) {
				Vector3 HPPosition = new Vector3 (Random.Range (-20, 20), 0, Random.Range (-20, 20));
				Instantiate (healthPack, HPPosition, Quaternion.identity);
				Vector3 APPosition = new Vector3 (Random.Range (-20, 20), 0, Random.Range (-20, 20));
				Instantiate (ammoPack, APPosition, Quaternion.identity);
			}
			yield return new WaitForSeconds (2.0f);
		}
	}
}
