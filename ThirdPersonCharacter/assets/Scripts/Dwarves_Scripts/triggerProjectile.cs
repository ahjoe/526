﻿using UnityEngine;
using System.Collections;

public class triggerProjectile : MonoBehaviour {

	public GameObject projectile;
	public Transform shootPoint;

	private GameObject magicMissile;
	public float attackLenght;
	public float attackRange;

	public GameObject hitEffect;

	public GameObject hideObject;
	WalkingOrc walkingOrc;
	void Start(){
		walkingOrc = gameObject.GetComponentsInParent<WalkingOrc> ()[0];
	}
	public void shoot()
	{
		magicMissile = Instantiate(projectile, shootPoint.position, transform.rotation) as GameObject;
		//CapsuleCollider sc = gameObject.AddComponent<CapsuleCollider>(  ) as CapsuleCollider;
		//sc.isTrigger = true;
		StartCoroutine(lerpyLoop(magicMissile));
	}

	// shoot loop
	public IEnumerator lerpyLoop(GameObject projectileInstance)
	{
		if (hideObject)
			hideObject.SetActive(false);
		var target = walkingOrc.GetTarget ();
		Vector3 victim = new Vector3(0,0,0);
		if (target == null) {
			victim = transform.position + transform.forward * attackRange;
		} else {
			victim = target.position;
		}

		float progress = 0;
		float timeScale = 1.0f / attackLenght;
		Vector3 origin = projectileInstance.transform.position;

		// lerp ze missiles!
		while (progress < 1)
		{
			if (projectileInstance)
			{			
				progress += timeScale * Time.deltaTime;
				float ypos = (progress - Mathf.Pow(progress, 2)) * 2;
				float ypos_b = ((progress + 0.1f) - Mathf.Pow((progress + 0.1f), 2)) * 12;
				projectileInstance.transform.position = Vector3.Lerp(origin, victim, progress) + new Vector3(0, ypos, 0);
				if (progress < 0.9f)
				{
					projectileInstance.transform.LookAt(Vector3.Lerp(origin, victim, progress + 0.1f) + new Vector3(0, ypos_b, 0));
				}
				yield return null;
			}
		}

		Destroy(projectileInstance);

		if (hitEffect)
			Instantiate(hitEffect, victim, transform.rotation);

		if (hideObject)
			hideObject.SetActive(true);

		yield return null;
	}

	public void clearProjectiles()
	{
		if (magicMissile)
			Destroy(magicMissile);
	}

}
