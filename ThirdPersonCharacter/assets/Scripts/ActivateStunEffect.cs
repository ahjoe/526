﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.CrossPlatformInput;

public class ActivateStunEffect : MonoBehaviour {

	public GameObject stunEffect;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per fram

	public void Activate() {
		stunEffect.SetActive (true);
		StartCoroutine(Deactivate(2));
	}

	IEnumerator Deactivate(float time){
		yield return new WaitForSeconds(time);
		stunEffect.SetActive (false);
	}
}
