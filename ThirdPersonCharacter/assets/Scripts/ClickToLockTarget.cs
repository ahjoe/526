﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityStandardAssets.CrossPlatformInput;

public class ClickToLockTarget : MonoBehaviour {

	public GameObject targetLockedMark;
	WalkingOrc walkingOrc;
	GameObject targetLockedMarkInstance = null;
	Transform target;
	private RaycastHit hit;
	Dictionary<int, Vector2> startPos = new Dictionary<int, Vector2>();

	void Start(){
		walkingOrc = gameObject.GetComponent<WalkingOrc> ();
	}
	void Update() {
		
		// Track a single touch as a direction control.
		foreach(Touch touch in Input.touches){
			//			Debug.Log (Input.touchCount);
		


			// Handle finger movements based on touch phase.
			switch (touch.phase) {
			// Record initial touch position.
			case TouchPhase.Began:
				Vector2 startPos1;
				startPos1 = touch.position;

				startPos.Add (touch.fingerId, startPos1);
				break;

			
			case TouchPhase.Moved:
				break;
				// Report that a direction has been chosen when the finger is lifted.
			case TouchPhase.Ended:
					//			Debug.Log (direction);
				Vector2 startPos2;
				if (!startPos.TryGetValue (touch.fingerId, out startPos2)) {
					break;
				}
				startPos.Remove (touch.fingerId);

				Vector2 direction = touch.position - startPos2;

				if(Mathf.Abs(direction.x) < 10 && Mathf.Abs(direction.y) < 5){
					Ray ray = Camera.main.ScreenPointToRay (touch.position);

					Debug.DrawRay (ray.origin, ray.direction * 30, Color.yellow);
					if (Physics.Raycast (ray, out hit, 30.0f)) {
						if (hit.transform.tag == "Enemy") {
							//					Debug.Log (hit.transform.name);	
							Transform transform = hit.transform;
							//					Vector3 position = transform.position;
							//					Vector3 temp = new Vector3 (45, 45, 45);
							//					position.y += 3.5f;
							//					GameObject t = Instantiate (targetLockedMark, position, Quaternion.Euler(temp)) as GameObject;
							var detectHit = transform.GetComponent<detectHit_Enemy>();
							//					if (targetLockedMarkInstance != null) {
							//						Destroy (targetLockedMarkInstance);
							//					}
							if (target != null) {
								var detectHit2 = target.GetComponent<detectHit_Enemy> ();
								detectHit2.Unlock ();
							}
							//					targetLockedMarkInstance = t;
							detectHit.Lock();
							target = transform;
							walkingOrc.SetTarget (target);
							//				hit.transform.
							//				walkingOrc.Attack ();
						}
					}
				}

				break;
			}

		}

//		if (Input.touchCount > 0) {
//			Ray ray = Camera.main.ScreenPointToRay (Input.GetTouch(0).position);
//		if (Input.GetMouseButtonUp(0)){
//		Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
//
//			Debug.DrawRay (ray.origin, ray.direction * 30, Color.yellow);
//			if (Physics.Raycast (ray, out hit, 30.0f)) {
//				if (hit.transform.tag == "Enemy") {
////					Debug.Log (hit.transform.name);	
//					Transform transform = hit.transform;
////					Vector3 position = transform.position;
////					Vector3 temp = new Vector3 (45, 45, 45);
////					position.y += 3.5f;
////					GameObject t = Instantiate (targetLockedMark, position, Quaternion.Euler(temp)) as GameObject;
//					var detectHit = transform.GetComponent<detectHit_Enemy>();
////					if (targetLockedMarkInstance != null) {
////						Destroy (targetLockedMarkInstance);
////					}
//					if (target != null) {
//						var detectHit2 = target.GetComponent<detectHit_Enemy> ();
//						detectHit2.Unlock ();
//					}
////					targetLockedMarkInstance = t;
//					detectHit.Lock();
//					target = transform;
//					walkingOrc.SetTarget (target);
//	//				hit.transform.
//	//				walkingOrc.Attack ();
//				}
//			}
//		}

//		bo	ol attack = CrossPlatformInputManager.GetButton("Attack");
//		if (attack) {
//			walkingOrc.Attack ();
//		}

	}
}


