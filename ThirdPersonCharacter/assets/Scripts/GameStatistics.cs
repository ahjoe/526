﻿using UnityEngine;
using System.Collections;

public class GameStatistics : MonoBehaviour {
	private static GameStatistics mySta = null;
	public Hashtable ht;
	public static GameObject obj;
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public static GameStatistics getInstance(){
		if (mySta == null) {
			obj = new GameObject ("Statistics");
			mySta = obj.AddComponent (typeof(GameStatistics)) as GameStatistics;
			mySta.init();
			DontDestroyOnLoad (obj);
		}
		return mySta;
	}
	private void init(){
		ht = new Hashtable ();
		ht.Add ("kills", 0);
		ht.Add ("ammo", 0);
		ht.Add ("level", 0);
		ht.Add ("wound_made",0);
		ht.Add ("wound_get",0);
	}
	public void resetHT(){
		ht.Clear ();
		ht.Add ("kills", 0);
		ht.Add ("ammo", 0);
		ht.Add ("level", 0);
		ht.Add ("wound_made",0);
		ht.Add ("wound_get",0);
	}
}
