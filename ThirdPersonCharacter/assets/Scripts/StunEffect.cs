﻿using UnityEngine;
using System.Collections;

public class StunEffect : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (other.tag == "Enemy") {
			var detectHit = other.GetComponent<detectHit_Enemy> ();
			detectHit.Stun ();
		}
			
	}
}
