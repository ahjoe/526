﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StatisticsShow : MonoBehaviour {

	public Text statisticsText;

	private Hashtable mysta; //= GameStatistics.getInstance ().ht;

	// Use this for initialization
	void Start () {
		mysta = GameStatistics.getInstance ().ht;
		int kills = (int)mysta ["kills"];
		int ammo = (int)mysta["ammo"];
		int wound_made = (int)mysta["wound_made"];
		int would_get = (int)mysta ["wound_get"];
		int level=(int)mysta["level"];
		int total = 100 * kills + 10 * ammo + 10 * would_get + wound_made + level;
		string content = "Enemy Killed: " + kills + "\nAmmo Get: " + ammo + "\nDamage Made: " + wound_made + "\n\nTOTAL: "+total;
		statisticsText.text = content;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
