﻿using UnityEngine;
using System.Collections;

public class BombExplosionEmit : MonoBehaviour {

	public float lifetime;

	void Update () {

		lifetime -= Time.deltaTime;
		var cc = gameObject.GetComponent<CapsuleCollider> () as CapsuleCollider;
		if (lifetime <= 0)
			cc.enabled = false;
	}
}
