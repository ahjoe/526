﻿using UnityEngine;
using System.Collections;

public class NoRotation : MonoBehaviour {
	Vector3 defaultDirection;
	// Use this for initialization
	void Start () {
		defaultDirection = this.transform.forward;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.forward = defaultDirection;
	}
}
