﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class detectHit_Main : MonoBehaviour {

	public bool stop;
	public Slider healthbar;
	private int ammoNum;
	Animator anim;
	public WalkingOrc wo;
	public Text Ammo_Left;
	//public GameStatistics gamestatistics;

	private float mainCharacterRate;
	void OnTriggerEnter(Collider other)
	{
		GameStatistics mysta = GameStatistics.getInstance ();
		//Debug.Log ("main:health=" + healthbar.value+" stop=" + stop+" other.tag="+other.tag);
		//Debug.Log ("anim="+anim.ToString());
		//Debug.Log("other.tag="+other.tag);
		//Debug.Log("other="+other.ToString());
		//Debug.Log("other="+this.ToString());
		anim = GetComponentInChildren<Animator>();
		if (other.tag != "Enemy_Weapon")
			return;
		healthbar.value -= mainCharacterRate*20;
		mysta.ht ["wound_get"]=((int)mysta.ht ["wound_get"])+1;
		if (!stop) {
			if (healthbar.value <= 0) {
				stop = true;
				anim.SetTrigger ("isDead");
			} else {
				stop = true;
				anim.SetBool ("isDamage", stop);
				StartCoroutine (DoAnimation ());
			}
		}
	}
	// Use this for initialization
	void Start () {
		anim = GetComponentInChildren<Animator>();
		//anim = wo.animation;
		stop = false;
		ammoNum = 10;
	}
	
	// Update is called once per frame
	void Update () {	
		// set up main character damage rate
		switch(wo.currentChar){
		case 0:
			mainCharacterRate = 0.5f;
			break;
		default:
		case 1:
			mainCharacterRate = 1f;	
			break;
		case 2:
			mainCharacterRate = 1.5f;
			break;
		}
	}

	IEnumerator DoAnimation()
	{
		//Debug.Log("Tada.");
		yield return new WaitForSeconds(1f); // wait for two seconds.
		//Debug.Log("This happens 0.1 seconds later. Tada.");
		stop = false;
		anim.SetBool ("isDamage", stop);
	}

	public bool Ammo_Check(){
		return ammoNum > 0;
	}

	public void Ammo_Dec(){
		if (wo.currentChar != 2 || wo.superPowerProgressBool) {
			ammoNum--;
			Ammo_Left.text = "Ammor Left: " + ammoNum;
		}
	}

	public void Ammo_Inc(int ammo){
		ammoNum += ammo;
		Ammo_Left.text = "Ammor Left: " + ammoNum;
		GameStatistics mysta = GameStatistics.getInstance ();
		mysta.ht["ammo"] = ((int)mysta.ht["ammo"])+ammo;
	}

	public void Ammo_Disappear(){
		Ammo_Left.text = "";
	}

	public void Ammo_Appear(){
		Ammo_Left.text = "Ammor Left: " + ammoNum;
	}
}
