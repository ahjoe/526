﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class detectHit_Enemy : MonoBehaviour {

	public bool stop;
	public Slider healthbar;
	Animator anim;
	bool isLocked = false;
//	GameObject targetLockedMarkInstance = null;
	public GameObject target;
	public WalkingOrc wo;
	private int damageOnEmeny;
	public GameObject stunMarker;
	private chase chase;
	//public GameStatistics gamestatistics;
	void OnTriggerEnter(Collider other)
	{
		//Debug.Log ("emeny:thealth=" + healthbar.value+" stop=" + stop+" other.tag="+other.tag);
		GameStatistics mysta = GameStatistics.getInstance();
		if (other.tag != "Main_Weapon")
			return;
		if (wo.currentChar == 1 && !wo.superPowerProgressBool) {
			healthbar.value = 0;
		}else{
			healthbar.value -= damageOnEmeny;
		}
		mysta.ht ["wound_made"] = ((int)mysta.ht ["wound_made"])+1;
		if (healthbar.value <= 0){
			Unlock ();
			stop = true;
			anim.SetBool ("isDead", stop);
			mysta.ht ["kills"] = ((int) mysta.ht ["kills"])+1;
			StartCoroutine(DeadAnimation());
		}
		else
		{
			stop = true;
			anim.SetBool ("isDamage", stop);
			mysta.ht ["wound_made"] = ((int)mysta.ht ["wound_made"])+1;
			StartCoroutine(DoAnimation());
		}
	}
	// Use this for initialization
	void Start () {
		anim = GetComponentInChildren<Animator>();
		stop = false;
		damageOnEmeny = 20;
		chase = GetComponent<chase> ();
	}

	// Update is called once per frame
	void Update () {	

	}

	IEnumerator DoAnimation()
	{
		yield return new WaitForSeconds(0.1f);
		stop = false;
		anim.SetBool ("isDamage", stop);
	}

	IEnumerator DeadAnimation(float sec = 4f)
	{
		yield return new WaitForSeconds(sec);
		stop = false;
		anim.SetBool ("isDamage", stop);
		Destroy (gameObject);
	}

	public void Lock(){
		if (!isLocked) {
//			Transform transform = gameObject.transform;
//			Vector3 position = transform.position;
//			Vector3 temp = new Vector3 (45, 45, 45);
//			position.y += 3.5f;
//			targetLockedMarkInstance = Instantiate (targetLockedMark, position, Quaternion.Euler (temp)) as GameObject;
			target.SetActive(true);
			isLocked = true;
		}
	}

	public void Unlock(){
		if (isLocked) {
			isLocked = false;
//			Destroy (targetLockedMarkInstance);
			target.SetActive(false);
		}
	}

	public void Stun(){
		Debug.Log("Stun");
		stunMarker.SetActive (true);
		chase.SetStop(true);
		anim.SetBool ("isWalk", false);
		anim.SetBool ("isAttack", false);
		anim.SetBool("isIdle", true);
		StartCoroutine(DeactivateStun(10));

	}

	IEnumerator DeactivateStun(float time){
		yield return new WaitForSeconds(time);
		chase.SetStop(false);
		//		anim.SetBool("isIdle", true);
		stunMarker.SetActive (false);
	}

}

