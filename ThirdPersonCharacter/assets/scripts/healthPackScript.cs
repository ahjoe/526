﻿using UnityEngine;
using System.Collections;

public class healthPackScript : MonoBehaviour {
	public float lifetime;
	//public gameObject hp;
	// Use this for initialization
	void Start () {
		Destroy(gameObject,lifetime);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Player") {
			//print ("player enter");
			detectHit_Main dm = other.gameObject.GetComponent<detectHit_Main>() as detectHit_Main;
			dm.healthbar.value += 20;
			//print ("begin:" + (dm.healthbar.value - 20) + " end:" + dm.healthbar.value);
			Destroy(gameObject);
		}

	}
}
