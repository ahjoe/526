﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class chase : MonoBehaviour {
	public Transform player;
	private Animator anim;
	public Slider slider;
	public Slider main_slider;
	private GameObject enemy_weapon;

	private bool stop;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		stop = false;

		//enemy_weapon.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		float speedRate = 0;
		bool dead = stop|anim.GetBool ("isDead");
		if (main_slider.value <= 0) {
			anim.SetBool ("isIdle", true);
			anim.SetBool ("isWalk", false);
			anim.SetBool ("isAttack", false);
			WeaponActive (false);
			return;
		}
		if (!dead) {
			Vector3 direction = player.position - this.transform.position;
			float angle = Vector3.Angle (direction, this.transform.forward);

			//print ("distance="+Vector3.Distance (player.position, this.transform.position));
			//print ("angle=" + angle);
			if (slider.value == 0) {
				anim.SetBool ("isIdle", true);
				anim.SetBool ("isWalk", false);
				anim.SetBool ("isAttack", false);
				WeaponActive (false);
			} else {
				
				if (Vector3.Distance (player.position, this.transform.position) < 40 && angle < 90) {
					direction.y = 0;
					this.transform.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.LookRotation (direction), 0.1f);
					anim.SetBool ("isIdle", false);
					if (direction.magnitude < 3) {
						anim.SetBool ("isWalk", false);
						anim.SetBool ("isAttack", true);
						stop = true;
						WeaponActive (true);
						StartCoroutine (DoAnimation ());
					} else {
						this.transform.Translate (0, 0, 0.025f);
						speedRate = 1;
						anim.SetBool ("isAttack", false);
						anim.SetBool ("isWalk", true);
						WeaponActive (false);
					}
				} else {
					anim.SetBool ("isIdle", true);
					anim.SetBool ("isWalk", false);
					anim.SetBool ("isAttack", false);
					WeaponActive (false);
				}
				// + new Vector3(0, GetComponent<Rigidbody>().velocity.y, 0);
			}
		} 
		//this.GetComponent<Rigidbody> ().velocity = transform.forward * speedRate + new Vector3(0, GetComponent<Rigidbody>().velocity.y, 0);;
	}

	void WeaponActive(bool active){
		
		enemy_weapon = this.transform.Find ("Bip001/Bip001 Prop1/Sphere_Weapon").gameObject;//GameObject.FindWithTag ("Enemy_Weapon");//Find ("Bip001/Bip001 Prop1/Sphere_Weapon");//.FindWithTag ("Enemy_Weapon");
		enemy_weapon.SetActive (active);
		//print(enemy_weapon.ToString()+" active="+enemy_weapon.activeSelf+" activeInHi+"+enemy_weapon.activeInHierarchy);
	}

	IEnumerator DoAnimation(float sec = 0.5f)
	{
		yield return new WaitForSeconds(sec);
		stop = false;
	}

	public void SetStop(bool stop){
		this.stop = stop;
	}
}
