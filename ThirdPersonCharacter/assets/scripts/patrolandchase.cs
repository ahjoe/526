﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class patrolandchase : MonoBehaviour {
	public Transform player;

	private Animator anim;
	private Vector3 nextPoint;
	private bool stop;
	public detectHit_Enemy dh;
	public Slider main_slider;
	public GameObject enemy_weapon_Right;
	public GameObject enemy_weapon_Left;
	// Use this for initialization
	void Start () {
		anim = GetComponent<Animator> ();
		stop = false;
	}

	// Update is called once per frame
	void Update () {
		if (stop) {
			WeaponActive (false);
			return;
		}
		if (main_slider.value <= 0) {
			anim.SetBool ("isWalk", true);
			anim.SetBool ("isRun", false);
			anim.SetBool ("isAttack", false);
			WeaponActive (false);
			if (Vector3.Distance (nextPoint, this.transform.position) < 0.5f) nextPoint = new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f))+this.transform.position;
			GotoNextPoint();
			return;
		}
		Vector3 direction = player.position - this.transform.position;
		float angle = Vector3.Angle (direction, this.transform.forward);
		//print ("distance="+Vector3.Distance (player.position, this.transform.position));
		//print ("angle=" + angle+" stop="+stop);
		stop = dh.stop | stop;
		if (stop) {
			WeaponActive (false);
			return;
		}
		if (Vector3.Distance (player.position, this.transform.position) < 100 && angle < 30) {
			direction.y = 0;
			float speedRate = 0f;
			this.transform.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.LookRotation (direction), 0.1f);
			anim.SetBool ("isWalk", false);
			if (direction.magnitude < 2) {
				anim.SetBool ("isRun", false);
				anim.SetBool ("isAttack", true);
				stop = true;
				WeaponActive (true);
				StartCoroutine(DoAnimation());
			} else {
				anim.SetBool ("isAttack", false);
				anim.SetBool ("isRun", true);
				WeaponActive (false);
				this.transform.Translate (0, 0, 0.08f);
				speedRate = 1f;
			}
			//this.GetComponent<Rigidbody> ().velocity = transform.forward * 5 * speedRate;
		} else {
			anim.SetBool ("isWalk", true);
			anim.SetBool ("isRun", false);
			anim.SetBool ("isAttack", false);
			WeaponActive (false);
			if (Vector3.Distance (nextPoint, this.transform.position) < 0.5f) nextPoint = new Vector3(Random.Range(-10.0f, 10.0f), 0, Random.Range(-10.0f, 10.0f))+this.transform.position;
			GotoNextPoint();
		}
	}

	void GotoNextPoint() {
		Vector3 direction = nextPoint - this.transform.position;
		//float angle = Vector3.Angle (direction, this.transform.forward);
		direction.y = 0;
		this.transform.rotation = Quaternion.Slerp (this.transform.rotation, Quaternion.LookRotation (direction), 0.1f);
		//this.GetComponent<Rigidbody> ().velocity = transform.forward * 3 + new Vector3(0, GetComponent<Rigidbody>().velocity.y, 0);;
		this.transform.Translate (0, 0, 0.03f);
	}

	void WeaponActive(bool active){
		//print(this.ToString()+" active="+active);
		enemy_weapon_Right = this.transform.Find ("Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/R_hamer/Sphere_Orc_Right_Hammer").gameObject;//GameObject.Find ("Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand/R_hamer/Sphere_Orc_Right_Hammer");
		enemy_weapon_Left = this.transform.Find ("Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 L Clavicle/Bip001 L UpperArm/Bip001 L Forearm/Bip001 L Hand/L_hamer/Sphere_Orc_Left_Hammer").gameObject;//GameObject.Find ("Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Spine2/Bip001 Neck/Bip001 L Clavicle/Bip001 L UpperArm/Bip001 L Forearm/Bip001 L Hand/L_hamer/Sphere_Orc_Left_Hammer");
		//print(enemy_weapon_Right.ToString()+" activeSelf="+enemy_weapon_Right.activeSelf+" activeInHi"+enemy_weapon_Right.activeInHierarchy);
		//print(enemy_weapon_Left.ToString()+" active="+enemy_weapon_Left.activeSelf+" activeInHi+"+enemy_weapon_Left.activeInHierarchy);
		enemy_weapon_Right.SetActive (active);
		enemy_weapon_Left.SetActive (active);
	}

	IEnumerator DoAnimation(float sec = 3f)
	{
		//Debug.Log("Tada.");
		yield return new WaitForSeconds(sec); // wait for two seconds.
		//Debug.Log("This happens 0.1 seconds later. Tada.");
		stop = false;
		anim.SetBool ("isDamage", stop);
	}
}
