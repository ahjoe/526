﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Loader : MonoBehaviour {
	AsyncOperation ao;
	public Slider progBar;
	public Text loadText;

	public bool isFakeLoadingBar = false;
	public float fakeIncrement = 0f;
	public float fakeTiming = 0f;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void SyncLoacScene(int level){
		SceneManager.LoadScene(level);
	}
    public void AsycLoadScene(int level)
    {
		progBar.gameObject.SetActive (true);
		loadText.gameObject.SetActive (true);
		loadText.text = "Loading...";
		GameStatistics.getInstance ().resetHT ();
		if (!isFakeLoadingBar) {
			StartCoroutine (LoadLevelWithRealProgress (level));
		} else {
		
		}
    }
	IEnumerator LoadLevelWithRealProgress(int level){
		yield return new WaitForSeconds (1);

		ao = SceneManager.LoadSceneAsync (level);
		ao.allowSceneActivation = false;
		while (!ao.isDone) {
			progBar.value = ao.progress;
			if (ao.progress >= 0.9f)
				ao.allowSceneActivation  = true;
			print("prog="+ao.progress);
			yield return null;
		}
	}
}
